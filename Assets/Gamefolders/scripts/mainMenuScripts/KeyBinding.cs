﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Mime;
using UnityEngine.UI;


public class KeyBinding : MonoBehaviour
{
    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    public Text up, left, right, down;
    public Text errorText;
    private GameObject currentKey;
    private Color32 normal = new Color32(255,175,66,255);
    private  Color32 selected = new Color32(200,200,200,255);
    
    void Start()
    {
        errorText.text = "";
        keys.Add("up", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("up","W")));
        keys.Add("down", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("down","S")));
        keys.Add("left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("left","A")));
        keys.Add("right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("right","D")));

        up.text = keys["up"].ToString();
        down.text = keys["down"].ToString();
        left.text = keys["left"].ToString();
        right.text = keys["right"].ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(keys["up"]))
        {
           Debug.Log("up"); 
        }
        if (Input.GetKeyDown(keys["down"]))
        {
            Debug.Log("down"); 
        }
        if (Input.GetKeyDown(keys["right"]))
        {
            Debug.Log("right"); 
        }
        if (Input.GetKeyDown(keys["left"]))
        {
            Debug.Log("left"); 
        }
    }

    void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;
            if (e.isKey)
            {
                if (e.keyCode == keys["right"] || e.keyCode == keys["left"] || e.keyCode == keys["up"] ||
                    e.keyCode == keys["down"])
                {
                    errorText.text = "Cette touche est déjà assignée, veuillez en choisir une autre !";
                    StartCoroutine(textchanger());
                    currentKey.GetComponent<Image>().color = normal;
                    currentKey = null;
                }

                if (currentKey != null)
                {
                    keys[currentKey.name] = e.keyCode;
                    currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                    currentKey.GetComponent<Image>().color = normal;
                    currentKey = null;
                }
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        if (currentKey != null)
        {
            currentKey.GetComponent<Image>().color = normal;
        }
       
        currentKey = clicked;
        currentKey.GetComponent<Image>().color = selected;

    }

    IEnumerator textchanger()
    {
        yield return new WaitForSeconds(1.5f);
        errorText.text = "";
    }

    public void saveKeys()
    {
        foreach (var key in keys)
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString() );
        }
        PlayerPrefs.Save();
    }
    
}
