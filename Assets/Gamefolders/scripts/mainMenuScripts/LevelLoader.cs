﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;
    public Text finishText;
    public PhotonButtons pb;
    public GameObject mainPlayer;
    public void Awake()
    {
        if (!FindObjectOfType<LevelLoader>())
        {
            DontDestroyOnLoad(this.transform);
            PhotonNetwork.sendRate = 30;
            PhotonNetwork.sendRateOnSerialize = 20;
        }
       
       // SceneManager.sceneLoaded += OnSceneFinishLoading;
    }

    public void Start()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    public void LoadLevel()
    {
        StartCoroutine(LoadAsynchronously());
    }

    IEnumerator LoadAsynchronously()
    {
        AsyncOperation operation = PhotonNetwork.LoadLevelAsync("Aire de jeu");
        //operation.allowSceneActivation = false;
        if (loadingScreen != null)
        {
            loadingScreen.SetActive(true);
            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress / 0.9f);
                slider.value = progress;
                progressText.text = (int) (progress * 100f) + "%";
                Debug.Log(operation.progress);
                yield return null;
            }
        }
    }

    public void CreateNewRoom()
    {
        PhotonNetwork.CreateRoom(pb.createRoom.text, new RoomOptions() {MaxPlayers = 2}, null);
    }

    public void JoinOrCreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;
        PhotonNetwork.JoinOrCreateRoom(pb.joinRoom.text, roomOptions, TypedLobby.Default);
    }

 /*   private void OnSceneFinishLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Aire de jeu")
        {
            spawnPlayer();
        }
    }

    private void spawnPlayer()
    {
        PhotonNetwork.Instantiate(mainPlayer.name,mainPlayer.transform.position, mainPlayer.transform.rotation,0);
    }
*/
    private void OnJoinedRoom()
    {
        LoadLevel();
    }
}
