﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkController : MonoBehaviour
{
   public GameObject sectionView1, sectionView2;

   public void Awake()
   {
      PhotonNetwork.ConnectUsingSettings("0.1");
      Debug.Log("connectiong to Photon..");
      PlayerPrefs.SetInt("HighScore", 0);
      PlayerPrefs.SetString("winner", "");
      PlayerPrefs.SetInt("scoreJ1", 0);
      PlayerPrefs.SetInt("scoreJ2", 0);
      PlayerPrefs.SetString("winner", "");
   }

   private void OnConnectedToMaster()
   {
      PhotonNetwork.JoinLobby(TypedLobby.Default);
      Debug.Log("connected to master");
   }

   private void OnJoinedLobby()
   {
      Debug.Log("On Joined lobby");
      sectionView2.SetActive(false);
      sectionView1.SetActive(true);
   }

   private void OnDisconnectedFromPhoton()
   {
      if (sectionView1.active)
      {
         sectionView1.SetActive(false);
      }
      if (sectionView2.active)
      {
         sectionView2.SetActive(false);
      }
      sectionView1.SetActive(true);
      Debug.Log("disc from photon service");
   }

   private void OnFailedToConnectToPhoton()
   {
      
   }

   public void OnPlayClicked()
   {
      sectionView1.SetActive(false);
      sectionView2.SetActive(true);
   }
   
}
