﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class connectingscript : MonoBehaviour
{
    public void Awake()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");
        Debug.Log("connectiong to Photon..");
    }

    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("connected to master");
    }
    private void OnJoinedLobby()
    {
        Debug.Log("On Joined lobby");
        SceneManager.LoadScene("menu");
    }
}
