﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PhotonButtons : MonoBehaviour
{
    public GameObject player;
    public InputField joinRoom, createRoom;
    public LevelLoader lvlLoad;
    public void OnClickedCreateRoom()
    {
        lvlLoad.CreateNewRoom();
    }

    private void onClickedJoinRoom()
    {
        lvlLoad.JoinOrCreateRoom();
    }
}
