﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.EventSystems;
using UnityEngineInternal;
using UnityEngineInternal.Input;


public class PlayerControler : Photon.MonoBehaviour
{
    [SerializeField] private float movementspeed = 5.0f;
    public bool devtesting = false;
    public PhotonView photonView;
    private Vector3 selfPos;
    public GameObject plcm;
    public GameObject sceneCam;
    private float heading = 0;
    private void Awake()
    {
        if (!devtesting && photonView.isMine)
        {
            sceneCam = GameObject.Find("Main Camera");
            sceneCam.SetActive(false);
            plcm.SetActive(true);
        }
    }
    private void Update()
    {
        if (!devtesting)
        {
            if (photonView.isMine)
            {
                heading = 0;
                Move();
            }
            else
            {
                smoothNetMovement();
            }
        }
        else
        {
            Move();
        }
       
    }

    private void Move()
    {
        int forward = 0;
        Vector3 camF = new Vector3(0, 0, 1);//plcm.transform.forward;
        camF.y = 0;
        camF = camF.normalized;
        if (Input.GetKey((KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("up", "Z"))))
        {
            forward = 1;
        }
        if (Input.GetKey((KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("down", "S"))))
        {
        
             forward = -1;
        }
        if (Input.GetKey((KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("left", "Q"))))
        {
            heading = -1;
        }
        if (Input.GetKey((KeyCode) System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("right", "D"))))
        {
            heading = 1;
        }
        transform.Rotate(0,heading * Time.deltaTime * 180,0);
        transform.Translate(camF * forward * Time.deltaTime*movementspeed);
    }

    private void smoothNetMovement()
    {
        transform.position = Vector3.Lerp(transform.position, selfPos, Time.deltaTime*8);
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
            
        }
        else
        {
            selfPos = (Vector3) stream.ReceiveNext();
        }
    }
}
