﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Score : Photon.MonoBehaviour
{
    private float time = 60.0f;
    public Text window1;
    public Text timeWindow;
    private static int _joueur1 = 0;
    private static int _joueur2 = 0;
    private Text txt;
    public PhotonView photonView;
    
    // Start is called before the first frame update
    void Start()
    {
        time -= Time.deltaTime;
        window1.text = "Joueur 1 : " + _joueur1+ "                    Joueur 2 : "+ _joueur2;
        timeWindow.text = "temps restant : " + ((int)time).ToString();
        txt = window1;
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = "Joueur 1 : " + _joueur1 + "                    Joueur 2 : " + _joueur2;
        time -= Time.deltaTime;
        timeWindow.text = "temps restant : " + ((int)time).ToString();
        if (photonView.isMine)
        {
            window1.text = txt.text;
            //txt = window1;
            //synchText();
        }
        else
        {
            synchText();
        }
        if (time <= 0.0f)
        {
            int bestPlayer = Mathf.Max(PlayerPrefs.GetInt("scoreJ1"), PlayerPrefs.GetInt("scoreJ2"));
            if (PlayerPrefs.GetInt("Highscore") < bestPlayer)
            {
                PlayerPrefs.SetInt("HighScore",bestPlayer);
                if (bestPlayer == PlayerPrefs.GetInt("scoreJ1"))
                {
                    PlayerPrefs.SetString("winner", "J1");
                }
                else
                {
                    PlayerPrefs.SetString("winner", "J2");
                }
                
            }
            PhotonNetwork.LeaveRoom();
            PhotonNetwork.LoadLevel("endGame");
        }

    }

    public static void addScoreJ1()
    {
        _joueur1 += 1;
        if (PlayerPrefs.GetInt("scoreJ1", 0) < _joueur1)
        {
            PlayerPrefs.SetInt("scoreJ1", _joueur1);
        }
    }
    public static void addScoreJ2()
    {
        _joueur2 += 1;
        if (PlayerPrefs.GetInt("scoreJ2", 0) < _joueur2)
        {
            PlayerPrefs.SetInt("scoreJ2",_joueur2);
        }
    }
    void synchText()
    {
        //Debug.Log(transform.GetChild(0).GetComponent<Text>().text);
        transform.GetChild(0).GetComponent<Text>().text = txt.text;
    }
    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            List<int> liste = new List<int>();
            liste.Add(_joueur1);
            liste.Add(_joueur2);
            stream.SendNext(_joueur1);
            stream.SendNext(_joueur2);
            //stream.SendNext(time);
            
        }
        else
        {
            _joueur1 = (int)stream.ReceiveNext();
            _joueur2 = (int) stream.ReceiveNext();
            //txt.text = "Joueur 1 : " + _joueur1+ "                    Joueur 2 : "+ _joueur2;
        }
    }
}
