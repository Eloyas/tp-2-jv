﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class spawnPlayer : MonoBehaviour
{
    public GameObject mainPlayer;
    // Start is called before the first frame update
    void Awake()
    {
        
    }

    private void OnJoinedRoom()
    {
        if (PhotonNetwork.countOfPlayersInRooms == 0)
        {
            mainPlayer.name = "J1";
        }
        if (PhotonNetwork.countOfPlayersInRooms == 1)
        {
            mainPlayer.name = "J2";
        }
        if (PhotonNetwork.countOfPlayersInRooms == 2)
        {
            mainPlayer.name = "J3";
        }
        if (PhotonNetwork.countOfPlayersInRooms == 3)
        {
            mainPlayer.name = "J4";
        }
        PhotonNetwork.Instantiate(mainPlayer.name,mainPlayer.transform.position, mainPlayer.transform.rotation,0);
    }
}
