﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour
{
	public static bool GameIsPaused = false;
	public GameObject PauseMenuUI;	
	public GameObject inGameOptions;
	public GameObject inGameDisplay;
	public GameObject inGamecontrols;

	void Start()
	{
		Cursor.lockState = CursorLockMode.None;
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (!GameIsPaused){
				Pause();
			}
		}
	}

	public void Resume()
	{
		PauseMenuUI.SetActive(false);
		Time.timeScale = 1f;
		GameIsPaused = false;
	}
	void Pause()
	{
		PauseMenuUI.SetActive(true);
		Time.timeScale = 0f;
		GameIsPaused = true;
	}

	public void LoadMenu()
	{
		PhotonNetwork.LeaveRoom();
		GameIsPaused = false;
		Time.timeScale = 1f;
		SceneManager.LoadScene(("Menu"));
	}

	public void options()
	{
		PauseMenuUI.SetActive(false);
		inGameOptions.SetActive(true);
	}
	public void retourOption()
	{
		inGameOptions.SetActive(false);
		PauseMenuUI.SetActive(true);
	}
	public void retourdisplay()
	{
		inGameDisplay.SetActive(false);
		inGameOptions.SetActive(true);
	}
	public void retourcontrols()
	{
		inGamecontrols.SetActive(false);
		inGameOptions.SetActive(true);
	}

	public void selectDisplay()
	{
		inGameOptions.SetActive(false);
		inGameDisplay.SetActive(true);
	}
	public void selectControls()
	{
		inGameOptions.SetActive(false);
		inGamecontrols.SetActive(true);
	}
	public void windowManagement()
	{
		WindowsManager wm = new WindowsManager();
		
	}
	
	
	
	public void QuitGame()
	{
		Application.Quit();
	}
}
