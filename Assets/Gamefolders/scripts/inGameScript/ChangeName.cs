﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeName : MonoBehaviour
{
    public TextMeshProUGUI name;

    public void Start()
    {
        if (PhotonNetwork.countOfPlayersInRooms == 0)
        {
            name.text = "J1";
        }
        if (PhotonNetwork.countOfPlayersInRooms == 1)
        {
            name.text = "J2";
        }
        if (PhotonNetwork.countOfPlayersInRooms == 2)
        {
            name.text = "J3";
        }
        if (PhotonNetwork.countOfPlayersInRooms == 3)
        {
            name.text = "J4";
        }
    }
}
