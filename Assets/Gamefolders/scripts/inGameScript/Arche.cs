﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arche : Photon.MonoBehaviour
{
    public GameObject arche;
    public GameObject spawner; 
    private Score ScoreKeeper;

    public PhotonView photonView;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.AddComponent<PhotonView>();
        photonView = PhotonView.Get(this);
        spawner = GameObject.Find("SpawnerArches");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            PlayerPrefs.SetInt("scoreJ2", PlayerPrefs.GetInt("scoreJ2",0)+1);
            Score.addScoreJ2();
        }
        else if (other.CompareTag("Player"))
        {
            PlayerPrefs.SetInt("scoreJ1", PlayerPrefs.GetInt("scoreJ1",0)+1);
            Score.addScoreJ1();
        }

        Debug.Log("la : " + photonView);
        if (this != null && photonView.isMine)
        {
            GameObject parent = this.transform.parent.gameObject;
            spawner.GetComponent<SpawnerArches>().enumeration.Remove(parent);
            PhotonNetwork.Destroy(parent);
        }
    }
}
