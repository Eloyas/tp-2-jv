﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endGame : MonoBehaviour
{
    public TextMeshProUGUI winner;
    private void Start()
    {
        if (PlayerPrefs.GetString("winner") == "J1")
        {
            winner.text = "Le joueur 1 a Gagné avec : " + PlayerPrefs.GetInt("HighScore") + " points!";
        }
        else
        {
            winner.text = "Le joueur 2 a Gagné avec : " + PlayerPrefs.GetInt("HighScore") + " points!";
        }
    }

    public void retourMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void quit()
    {
        Application.Quit();
    }
}
