﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerArches : MonoBehaviour
{
    public GameObject arche;

    public List<GameObject> enumeration;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnObject()
    {
        float x = Random.Range(-14.0f, 14.0f);
        float z = Random.Range(-14.0f, 14.0f);
        int rotation = Random.value < .5 ? 0 : 90;
        arche.transform.position = new Vector3(x, 1.5f, z);
        arche.transform.rotation = Quaternion.Euler(0, rotation, 0);
        enumeration.Add(PhotonNetwork.Instantiate(arche.name,arche.transform.position, arche.transform.rotation,0));
        
        //nouveau.GetComponent<Arche>().spawner = this.gameObject;
        Debug.Log("Test");
    }
}
