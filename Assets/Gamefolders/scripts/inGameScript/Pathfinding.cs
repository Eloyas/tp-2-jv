﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    [SerializeField] private float movement_speed = 5.0f;
    [SerializeField] private float turn_speed = 180.0f;
    [SerializeField] private bool difficile;

    private List<GameObject> cibles;

    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        movement_speed *= Time.deltaTime;
        StartCoroutine(waiter(3));
        target = SelectTarget();
    }

    private void Awake()
    {
        cibles = GameObject.Find("SpawnerArches").GetComponent<SpawnerArches>().enumeration;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            target = SelectTarget();

        }
        else
        {
           transform.position = Vector3.MoveTowards(transform.position, target.transform.position, movement_speed);
        }
        

    }

    IEnumerator waiter(int delay)
    {
         yield return new WaitForSeconds(delay);
    }

    GameObject SelectTarget()
    {
        if (cibles.Count == 0)
        {
            return null;
        }
        if (difficile)
        {
            GameObject bestTarget = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            foreach(GameObject potentialTarget in cibles)
            {
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if(dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget;
                }
            }

            return bestTarget;
        }
        else
        {
           /*GameObject bestTarget = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = transform.position;
            foreach(GameObject potentialTarget in cibles)
            {
                float sum = 0.0f;
                foreach (GameObject voisin in cibles)
                {
                    
                    if (voisin != potentialTarget)
                    {
                        Vector3 distanceVoisin = potentialTarget.transform.position - voisin.transform.position;
                        double distance = Math.Sqrt(distanceVoisin.sqrMagnitude);
                        sum += (float) Math.Pow(15.0f - distance, 2.0f);
                    }
                }

                if (sum < closestDistanceSqr)
                    {
                        closestDistanceSqr = sum;
                        bestTarget = potentialTarget;
                    }
                
            }
            */
           GameObject bestTarget = cibles[UnityEngine.Random.Range (0, cibles.Count)];
                return bestTarget;
        }

        return cibles[0];
    }

 /*   void OnCollisionEnter(Collision collision)
    {
        target = SelectTarget();
    }
    */
/*
    void LookAtTarget(GameObject cible)
    {
        //Vector3 chemin = this.transform.position - cible.transform.position;
        //float angle = Vector3.Angle(chemin, transform.forward);
        
    }
    */
}
